#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <pybind11/embed.h>
#include "tvshow.h"

TEST_CASE("TvShow parse filename 1", "[tvshowfilenameparser]") {
	// Disable loggers one for all
	auto consolelog = spdlog::stdout_color_mt("console");
	auto pythonlog = spdlog::stdout_color_mt("python");
	consolelog->set_level(spdlog::level::off);
	pythonlog->set_level(spdlog::level::off);

	pybind11::scoped_interpreter python{};	// Embed python env

	TvShow tvshow("altered.carbon.s01e03.720p.webrip.hevc.x265.rmteam.mkv");
	REQUIRE(tvshow.getFilename() == "altered.carbon.s01e03.720p.webrip.hevc.x265.rmteam");
	REQUIRE(tvshow.getTitle() == "altered carbon");
	REQUIRE(tvshow.getSeason() == "1");
	REQUIRE(tvshow.getEpisode() == "3");
	REQUIRE(tvshow.getQuality() == "webrip");
	REQUIRE(tvshow.getRipTeam() == "rmteam");
}

TEST_CASE("TvShow parse filename 2", "[tvshowfilenameparser]") {
	pybind11::scoped_interpreter python{};	// Embed python env

	TvShow tvshow("Marvel's.Jessica.Jones.S02E01.AKA.Start.at.the.Beginning.REPACK.720p.10bit.WEBRip.2CH.x265.HEVC-PSA.mkv");
	REQUIRE(tvshow.getFilename() == "Marvel's.Jessica.Jones.S02E01.AKA.Start.at.the.Beginning.REPACK.720p.10bit.WEBRip.2CH.x265.HEVC-PSA");
	REQUIRE(tvshow.getTitle() == "Marvel's Jessica Jones");
	REQUIRE(tvshow.getSeason() == "2");
	REQUIRE(tvshow.getEpisode() == "1");
	REQUIRE(tvshow.getQuality() == "WEBRip");
	REQUIRE(tvshow.getRipTeam() == "PSA");
}

TEST_CASE("TvShow parse filename 3", "[tvshowfilenameparser]") {
	pybind11::scoped_interpreter python{};	// Embed python env

	TvShow tvshow("11.22.63.S01E01.720p.HDTV.X264-DIMENSION[ettv].mkv");
	REQUIRE(tvshow.getFilename() == "11.22.63.S01E01.720p.HDTV.X264-DIMENSION[ettv]");
	REQUIRE(tvshow.getTitle() == "11 22 63");
	REQUIRE(tvshow.getSeason() == "1");
	REQUIRE(tvshow.getEpisode() == "1");
	REQUIRE(tvshow.getQuality() == "HDTV");
	REQUIRE(tvshow.getRipTeam() == "DIMENSION");
}