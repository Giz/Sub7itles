#ifndef _SUBSEEKER_
#define _SUBSEEKER_

/**
* \class		SubSeeker
* \brief		Class representing subtitles seeker
* \author		Damien P.
* \date			2018
*/

#include <unordered_map>
#include <vector>
#include <string>
#include <pybind11/pybind11.h>

class TvShow;
class Subtitle;

class SubSeeker
{
public:
	/**
	* \brief Constructor
	*
	* Creates the subtitles seeker
	*/
	SubSeeker(std::string pythonmodulespath);

	/**
	* \brief Retrieve subtitles
	*
	* Retrieve all subtitles of the tv show and return found subtitles
	*
	* \param tvshow : tv show
	* \return a vector of found subtitles
	*/
	std::vector<Subtitle> retrieveSubtitles(const TvShow& tvshow);
	/**
	* \brief Download subtitle
	*
	* Download subtitle of the tv show at index
	*
	* \param tvshow : tv show, index (-1 = autosearch for best subtitle)
	* \return true if ok, false otherwise
	*/
	bool downloadSubtitle(const TvShow& tvshow, int index = -1);

private:
	std::unordered_map<uint8_t, pybind11::module> m_PythonModules;		/**< Map of python modules */
};

#endif // _SUBSEEKER_
