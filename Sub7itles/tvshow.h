#ifndef _TVSHOW_
#define _TVSHOW_

/**
* \class		TvShow
* \brief		Class representing tv show
* \author		Damien P.
* \date			2018
*/

#include "subtitle.h"
#include <string>
#include <list>
#include <vector>

class TvShow
{
public:
	/**
	* \brief Constructor
	*
	* Creates the tv show
	*
	* \param filename : filename of the tv show
	*/
	TvShow(std::string filename);

	/**
	* \brief Filename getter
	*
	* Return the filename of the tv show
	*
	* \return filename
	*/
	const std::string& getFilename() const;
	/**
	* \brief Title getter
	*
	* Return the title of the tv show
	*
	* \return title
	*/
	const std::string& getTitle() const;
	/**
	* \brief Title urlized getter
	*
	* Return the title of the tv show in url format (spaces are replaced by '+')
	*
	* \return title urlized
	*/
	const std::string getTitleURLized() const;
	/**
	* \brief Season getter
	*
	* Return the season number of the tv show
	*
	* \return season
	*/
	const std::string& getSeason() const;
	/**
	* \brief Episode getter
	*
	* Return the episode number of the tv show
	*
	* \return episode
	*/
	const std::string& getEpisode() const;
	/**
	* \brief Rip team getter
	*
	* Return the rip team of the tv show
	*
	* \return rip team
	*/
	const std::string& getRipTeam() const;
	/**
	* \brief Codec getter
	*
	* Return the codec of the tv show
	*
	* \return codec
	*/
	const std::string& getCodec() const;
	/**
	* \brief Quality getter
	*
	* Return the quality of the tv show
	*
	* \return quality
	*/
	const std::string& getQuality() const;
	/**
	* \brief Resolution getter
	*
	* Return the resolution of the tv show
	*
	* \return resolution
	*/
	const std::string& getResolution() const;
	/**
	* \brief Alternative rip teams getter
	*
	* Return a list of alternative rip teams of the tv show
	*
	* \return alternative rip teams
	*/
	const std::list<std::string>& getAlternativeRipTeams() const;
	/**
	* \brief Subtitles getter
	*
	* Return a vector of subtitles of the tv show
	*
	* \return vector of subtitles
	*/
	const std::vector<Subtitle>& getSubtitles() const;

	/**
	* \brief Add subtitles
	*
	* Append a vector of subtitles to the tv show's vector of subtitles
	*
	* \param subtitles : a vector of subtitles
	*/
	void addSubtitles(std::vector<Subtitle> subtitles);

private:
	std::string m_Filename;										/**< Filename */
	std::string m_Title;										/**< Title */
	std::string m_Season;										/**< Season */
	std::string m_Episode;										/**< Episode */
	std::string m_RipTeam;										/**< Rip team */
	std::string m_Codec;										/**< Codec */
	std::string m_Quality;										/**< Quality */
	std::string m_Resolution;									/**< Resolution */
	std::list<std::string> m_AltRipTeams;						/**< List of alternative rip teams */
	std::vector<Subtitle> m_Subtitles;							/**< Vector of subtitles */
};

#endif // _TVSHOW_
