#include "subtitle.h"
#include "tvshow.h"
#include <regex>

Subtitle::Subtitle(std::string ripversion, std::string extrainfos, std::string downloadurl) :
	m_DownloadURL(downloadurl),
	m_PythonModuleID(0)
{
	m_RipVersion = ripversion.erase(ripversion.find_last_not_of("\t\n\v\f\r ") + 1).erase(0, ripversion.find_first_not_of("\t\n\v\f\r "));	// Trim
	m_ExtraInfos = extrainfos.erase(extrainfos.find_last_not_of("\t\n\v\f\r ") + 1).erase(0, extrainfos.find_first_not_of("\t\n\v\f\r "));	// Trim
}

const std::string& Subtitle::getRipVersion() const
{
	return m_RipVersion;
}

const std::string& Subtitle::getExtraInfos() const
{
	return m_ExtraInfos;
}

const std::string& Subtitle::getDownloadURL() const
{
	return m_DownloadURL;
}

const uint8_t Subtitle::getPythonModuleID() const
{
	return m_PythonModuleID;
}

void Subtitle::setPythonModuleID(uint8_t pythonmoduleid)
{
	m_PythonModuleID = pythonmoduleid;
}

const bool Subtitle::match(const TvShow& tvshow) const
{
	// Test rip team and quality
	if (std::regex_search(m_RipVersion, std::regex(".*(" + tvshow.getRipTeam() + ").*", std::regex_constants::icase)) || 
		std::regex_search(m_ExtraInfos, std::regex(".*(" + tvshow.getRipTeam() + ").*", std::regex_constants::icase)) ||
		std::regex_search(m_RipVersion, std::regex(".*(" + tvshow.getQuality() + ").*", std::regex_constants::icase)) ||
		std::regex_search(m_ExtraInfos, std::regex(".*(" + tvshow.getQuality() + ").*", std::regex_constants::icase)))
		return true;

	// Test alternative rip teams
	const std::list<std::string>& altripteams = tvshow.getAlternativeRipTeams();
	for (std::list<std::string>::const_iterator it = altripteams.begin(); it != altripteams.end(); ++it)
	{
		if (std::regex_search(m_RipVersion, std::regex(".*(" + (*it) + ").*", std::regex_constants::icase)) ||
			std::regex_search(m_ExtraInfos, std::regex(".*(" + (*it) + ").*", std::regex_constants::icase)))
			return true;
	}

	return false;
}

std::ostream& operator<<(std::ostream& os, const Subtitle& sub)
{
	os << "Rip Version : " << sub.getRipVersion() << std::endl << "Extra Infos : " << sub.getExtraInfos() << std::endl << "Download URL : " << sub.getDownloadURL();
	return os;
}
