#ifndef _SUBTITLE_
#define _SUBTITLE_

/**
* \class		Subtitle
* \brief		Class representing subtitle
* \author		Damien P.
* \date			2018
*/

#include <string>

class TvShow;

class Subtitle
{
public:
	/**
	* \brief Constructor
	*
	* Creates the subtitle
	*
	* \param ripversion : rip version of the subtitle
	* \param extrainfos : extra infos of the subtitle
	* \param downloadurl : url to query to download the subtitle
	*/
	Subtitle(std::string ripversion, std::string extrainfos, std::string downloadurl);

	/**
	* \brief Rip version getter
	*
	* Return the rip version of the subtitle
	*
	* \return rip version
	*/
	const std::string& getRipVersion() const;
	/**
	* \brief Extra infos getter
	*
	* Return the extra infos of the subtitle
	*
	* \return extra infos
	*/
	const std::string& getExtraInfos() const;
	/**
	* \brief Download url getter
	*
	* Return the download url of the subtitle
	*
	* \return download url
	*/
	const std::string& getDownloadURL() const;
	/**
	* \brief Python module ID getter
	*
	* Return the python module ID where the subtitle come from
	*
	* \return python module ID
	*/
	const uint8_t getPythonModuleID() const;

	/**
	* \brief Python module ID setter
	*
	* Set the python module ID where the subtitle come from
	*
	* \param pythonmoduleid : the python module ID in uint_8
	*/
	void setPythonModuleID(uint8_t pythonmoduleid);

	/**
	* \brief Match with Tv show
	*
	* Try to match subtitle with tv show
	*
	* \param tvshow : tv show
	* \return true if the subtitle matches with the tv show
	*/
	const bool match(const TvShow& tvshow) const;

private:
	std::string m_RipVersion;			/**< Rip version */
	std::string m_ExtraInfos;			/**< Extra infos */
	std::string m_DownloadURL;			/**< Download url */
	uint8_t m_PythonModuleID;			/**< Python module ID where the subtitle come from */
};

std::ostream& operator<<(std::ostream& os, const Subtitle& sub);

#endif // _SUBTITLE_
