# Sub7itles #

### What is this repository for? ###

#### Quick summary ####

Download subtitles from a filename.  
Very useful for getting subtitles from a tv show episode file.

#### How it works ####

You open Sub7itles with the command "Sub7itles *filename*" and you auto-magically get your subtitle !

#### How it really works ####

Sub7itles loads python modules to parse subtitles websites and gets desired subtitle.  
You can write your own module in python to parse whatever subtitles website you want.

### How do I get set up? ###

You must use a C++17 compiler to build Sub7itles (std Filesystem library).

#### Dependencies ####

* [pybind11](https://github.com/pybind/pybind11)
* [spdlog](https://github.com/gabime/spdlog)